﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LProject
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
        }
        private void ListBox_Loaded(object sender, RoutedEventArgs e)
        {
            var List = new List<string>();
            new List<string>() { "Вася Пупкин" };
            new List<string>() { "Иван Петрушкин" };
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var ListBox = sender as ListBox;
            string value = ListBox.SelectedItem as string;
            this.Title = "Selected: " + value;
        }
    }
}