﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace LProject
{
    class DBWork
    {
        private DataContext db;

        public DBWork()
        {
            db = new DataContext("Data Source=DESKTOP-KQI339F\\SQLEXPRESS;Initial Catalog=LProject;Integrated Security=True;Pooling=False");

            // Attach the log to show generated SQL.
            db.Log = Console.Out;
        }

        public List<string> getNames()
        {
            Table<Man> man = db.GetTable<Man>();

            //Insert
            Man newMan = new Man();
            newMan.Name = "Somename";
            man.InsertOnSubmit(newMan);

            db.SubmitChanges();

            //Select
            var nameQuery =
                from cust in man
                select cust.Name;

            List<string> names = new List<string>();

            foreach (var line in nameQuery)
            {
                names.Add(line);
            }

            return names;
        }
        
        
    }
}
