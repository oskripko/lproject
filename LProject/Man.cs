﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace LProject
{
    [Table(Name = "People")]
    class Man
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int ID;

        [Column]
        public string Name {
            get;
            set;
        }

        public Man()
        {

        }
    }
}
